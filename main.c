#include <avr/interrupt.h>
#include "softuart.h"

int main(void){
	char c;
	softuart_init(PORT_1);
	softuart_init(PORT_2);
	sei();
	for(;;){
//		c = softuart_getchar();
		softuart_putchar(PORT_1, 'a');
		softuart_putchar(PORT_2, 'a');
	}
	return 0;
}
